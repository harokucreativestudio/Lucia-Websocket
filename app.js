require('dotenv').config()
const WebSocket = require('./structures/WebSocket.js'); // Declare Websocket
const config = require('./config.json');
const Lucia = require('./structures/DiscordEvaluator.js'); // Declare Lucia

const lucia = new Lucia({
    disableEveryone: true,
    fetchAllMembers: true
});

var frameSocket = new WebSocket(lucia, config.port); // Run on Port 7500 by default, see ./config.json
frameSocket.registerRoots(); // Run Register Roots

require('./structures/libs/eventHandler')(lucia, frameSocket); // Event Handler for Lucia

lucia.login(process.env.TOKEN);