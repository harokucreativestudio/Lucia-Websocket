exports.route = async (lucia, socket, info, req, res) => {
    if(!req.query.q) return res.send({return: 'You\'re missing an argument, Goshujin-sama!'});
    try {
        return res.send({return: 'That said, your test API is working, Goshujin-sama!'});
    } catch(err) {
        return res.sendStatus(err.status);
    }
}

exports.info = {
    name: 'Test API',
    endpoint: '/test'
}