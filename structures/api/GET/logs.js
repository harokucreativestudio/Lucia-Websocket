const { readFileSync } = require('fs');

exports.route = async (lucia, socket, info, req, res) => {
	try{
		return res.send({ errLog: getLog() });
	}catch(err){
		return res.sendStatus(err.status);
	}
}

function getLog(){
	const logs = readFileSync('./logs/app.log', 'utf8');
	try{
		return JSON.parse(logs);
	}catch(err){
		return logs;
	}
}

exports.info = {
    name: 'Logging API',
    endpoint: '/logs'
}
