require('dotenv').config()
const path = require('path');
const baseDir = require('../src/pathfinder.js').baseDir;
const express = require('express');
const { urlencoded, json } = require('body-parser');
const logger = path.join(baseDir, 'structures/libs/logEngine.js');
const { 
	getRows, 
	connection,
	readEngine,
	getHeader,
	config,
	processHandler,
	catchAsync
} = require('./index');
const { join } = require('path');
const morgan = require('morgan');

class WebSocket {
	constructor(lucia, port) {
		this.connection = connection;
		this.rows = getRows;
		this.extConfig = config;
		this.lucia = lucia;
		this.catchAsync = catchAsync;
		this.app = express()
		this.app.set('views', join(__dirname, './../src/views'))
		this.app.set('view engine', 'hbs')
		this.app.use(express.static(join(__dirname, '../public')))
		this.app.use(urlencoded({ extended: false }))
		readEngine(this)
		this.app.use(json())
		this.app.use(morgan('combined', { stream: logger.stream }));
		this.app.use(function(err, req, res, next) {
			res.locals.message = err.message;
			res.locals.error = req.app.get('env') === 'development' ? err : {};
			logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);		  
			res.status(err.status || 500);
			res.render('error');
		})
		this.server = this.app.listen(port, () => {
			console.log(`${this.extConfig.nickname} Websocket is listening on port ${this.server.address().port}`);
		})
		processHandler()
	}

	registerRoots() { // Register Roots
		this.app.get('/', (req, res) => {
			res.render('index', { title: `${this.extConfig.nickname} | Lolization Webgate`, header: getHeader() })
		})
		require('./passport')(this.lucia, this) // Discord Oauth2 Passport
		require('./libs/APIHandler')(this.lucia, this); // API Handler for WebSocket
		require('./libs/routeHandler')(this.lucia, this); // Route Handler for WebSocket
		this.app.get('*', (req, res) => { // 404 Handler for Websocket
			res.render('error', { title: `${this.extConfig.nickname} Error Catcher`, errType: `404 Not Found` })
		})
	}
}

module.exports = WebSocket;
