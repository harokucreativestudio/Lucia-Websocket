require('dotenv').config()

module.exports = (lucia, frameSocket, message) => {
	try {
		if(!message.guild || message.author.bot) return;
		message.guild.prefix = lucia.prefix;
		if(!message.content.startsWith(lucia.prefix)) return;
		const extend = message.content.slice(lucia.prefix.length).trim().split(/ +/g);
		const command = extend.shift().toLowerCase();
		extend.socket = frameSocket;
		extend.norights = NoRights;
		extend.missing = MissingArgs;
		const plugin = lucia.commands.get(command) || lucia.commands.get(lucia.aliases.get(command));
		if(!plugin) return;
		if(!parseCmd(lucia, message, plugin.info, frameSocket)) return;
		return plugin.run(lucia, message, extend);
	} catch(err) {
		return message.channel.send(err.stack, { code: 'unu' });
	}
}

function parseCmd(lucia, message, plugin, socket){
	if(plugin.ownerOnly && !lucia.config.owners.includes(message.author.id)){
		message.channel.send(` ❌ **| ${message.author.toString()}, ${lucia.config.nickname} can't obey your command, Shikikan-san!**`);
		return false;
	}
	if(plugin.authorPerm.length){
		let perms = [];
		for(const perm of plugin.authorPerm){
			if(!message.member.hasPermission(perm.toUpperCase())) perms.push(perm);
		}
		if(perms.length){
			message.channel.send(`❌ **| ${message.author.toString()}, You lack ${perms.map(x => `▫ | ${x}`).join('\n')} permission nodes to do this, Shikikan-san! \n**`);
			return false;
		}
	}
	if(plugin.luciaPerm){
		let perms = [];
		for(const perm of plugin.luciaPerm){
			if(!message.guild.me.hasPermission(perm.toUpperCase())) perms.push(perm);
		}
		if(perms.length){
			message.channel.send(`❌ **| ${message.author.toString()}, ${lucia.config.nickname} don't have enough permissions to do this, Shikikan-san! \n** ${perms.map(x => `▫ | ${x}`).join('\n')}`);
			return false;
		}
	}
	return true;
}

function NoRights(lucia, message, info){
	return message.channel.send(`
❌ | You can't use \`${lucia.prefix}${info.name}\` against him, Shikikan-san!
	`);
}

function MissingArgs(lucia, message, reason, info){
	return message.channel.send(`
❌ | Invalid Syntax for \`${lucia.prefix}${info.name}\`, Shikikan-san!
▫ | Reason: **${reason}**
▫ | Correct usage: \`${lucia.prefix}${info.usage}\`
▫ | More help use: \`${lucia.prefix}help ${info.name}\`
	`);
}