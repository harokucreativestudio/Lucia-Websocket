require('dotenv').config()
module.exports = lucia => {
	console.log(`${lucia.user.username} Connected, Hooked with Websocket at ${lucia.config.dns}. System Fully Functionable..`);
	lucia.user.setActivity(`It wasn't my wish to be this cute...`, {type: 'LISTENING'});
	console.log(`Ready to engage, Websocket Connection on ${lucia.config.port}, with DB Sync at ${lucia.config.redis.host}:${lucia.config.redis.port}`);
}