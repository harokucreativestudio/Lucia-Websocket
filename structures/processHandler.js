function processHandler() {
    process.on('unhandledRejection', error => {
        console.error(`Unhandled Rejection Found!:`, error);
    });
};

module.exports = { processHandler };