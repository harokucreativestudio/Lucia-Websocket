exports.route = async (lucia, socket, info, req, res) => {
    const rawData = req.user;
    try {
        return res.render('profile', { 
        title: `${lucia.config.nickname} | Profile Page`, 
        userId: `${req.user.id}`,
        avatarId: `${req.user.avatar}`,
        username: `${req.user.username}`,
        email: `${req.user.email}`,
        verificationStatus: `${req.user.verified}`,
        locale: `${req.user.locale}`,
        mfa: `${req.user.mfa_enabled}`,
        discriminator: `${req.user.discriminator}`
    });
    } catch(err) {
        return res.sendStatus(err.status);
    }
	function checkAuth(req, res, next) {
		if (req.isAuthenticated()) return next();
		res.send({result: 'You\'re not logged in'});
    }
}

exports.info = {
    name: 'Discord Profile',
    route: 'profile'
}