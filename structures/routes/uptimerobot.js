exports.route = async (lucia, socket, info, req, res) => {
    try {
        return res.render('uptimerobot', { title: `${lucia.config.nickname} | Lolization Webgate`})
    } catch(err) {
        return res.render('error', { title: `${lucia.config.nickname} Error Catcher`, errType: err.status });
    }
}

exports.info = {
    name: 'Uptime Status',
    route: 'status'
}