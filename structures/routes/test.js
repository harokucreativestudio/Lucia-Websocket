exports.route = async (lucia, socket, info, req, res) => {
    if(!req.query.q) return res.send({return: 'You\'re missing an argument, Shikikan-san!'});
    try {
        return res.render('test', { title: `${lucia.config.nickname} | Lolization Webgate`, body: 'Your Route is working!' });
    } catch(err) {
        return res.sendStatus(err.status);
    }
}

exports.info = {
    name: 'Test',
    route: 'test'
}