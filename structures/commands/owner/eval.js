const { RichEmbed } = require('discord.js');

exports.run = async (lucia, message, extend) => {
	const now = Date.now();
	try {
		if(!extend.length) throw new TypeError('Shikikan no baka! You can\'t evaluate a blank script, Goshujin-sama! ');
		let code = extend.join(' ');
		let evaled = await eval(code);
		const type = (evaled === undefined || evaled === null) ? 'void' : evaled.constructor.name;
		if(typeof evaled !== 'string') evaled = require('util').inspect(evaled, { depth: 0 });
		evaled = evaled.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
		if(evaled.length > 2048) evaled = await hastebin(evaled)
		else evaled = 
		`\`\`\` ${evaled} \`\`\``;
		const embed = new RichEmbed()
		.setAuthor("Evaluate", lucia.user.displayAvatarURL)
		.setFooter('This bot is powered by Lolization LLC', 'http://download.lolization.space/pict/loli54.png')
		.setTitle('Evaled Context ♪')
		.setColor('GREEN')
		.setDescription(evaled)
		.addField('type', 
		`\`\`\` ${type} \`\`\``)
		.setFooter(`⏱️ ${(Date.now()-now)/1000}s`);
		return message.channel.send(embed);
	} catch(err) {
		const embed = new RichEmbed()
		.setAuthor("Ping", lucia.user.displayAvatarURL)
		.setFooter('This bot is powered by Lolization LLC', 'http://download.lolization.space/pict/loli54.png')
		.setColor('RED')
		.setTitle('🚫 Evaluate Error')
		.setDescription(
		`\`\`\` ${err} \`\`\``)
		.setFooter(`⏱️ ${(Date.now()-now)/1000}s`);
		return message.channel.send(embed);
	}
}

const { post } = require('node-superfetch');
async function hastebin(text){
	const { body } = await post('https://www.hastebin.com/documents').send(text);
	return `https://www.hastebin.com/${body.key}`;
}

exports.info = {
	name: 'eval',
	description: 'evaluate arbitary javascript',
	usage: 'eval <code>',
	aliases: ['ev'],
	ownerOnly: true,
	authorPerm: [],
	luciaPerm: ['EMBED_LINKS']
}