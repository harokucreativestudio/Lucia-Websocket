const { RichEmbed } = require('discord.js');
const { exec } = require('child_process');

exports.run = async (lucia, message, extend, err) => {
	const now = Date.now();
	try {
		if(err) throw err;
		if(!extend.join(' ')) throw 'Exec command cannot execute without input!. Goshujin-sama no bbbaka...';
		exec(extend.join(' '), async (stderr, stdout) => {
			if(stderr) return this.run(lucia, message, extend, stderr);
			if(!stdout.length) stdout = `\`\`\` \`\`\``
			else if(stdout.length > 2048) stdout = await hastebin(stdout)
			else stdout = 
			`\`\`\` ${stdout} \`\`\``;
			const embed = new RichEmbed()
			.setColor('GREEN')
			.setDescription(stdout)
			.setFooter(`⏱️ ${(Date.now()-now)/1000}s`);
			return message.channel.send(embed);
		});
	} catch(err) {
		err = err.stack || err;
		if(err.length > 2048) err = await hastebin(err)
		else err = 
		`\`\`\` ${err} \`\`\``;
		const embed = new RichEmbed()
		.setColor('RED')
		.setDescription(err)
		.setFooter(`⏱️ ${(Date.now()-now)/1000}s`);
		return message.channel.send(embed);
	}
}

const { post } = require('node-superfetch');
async function hastebin(text){
	const { body } = await post('https://www.hastebin.com/documents').send(text);
	return `https://www.hastebin.com/${body.key}`;
}

exports.info = {
	name: 'exec',
	description: 'execute bash command',
	usage: 'exec <code>',
	aliases: ['$'],
	ownerOnly: true,
	authorPerm: [],
	luciaPerm: ['EMBED_LINKS', 'ATTACH_FILES']
}