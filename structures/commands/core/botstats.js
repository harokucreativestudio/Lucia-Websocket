const { RichEmbed, version } = require('discord.js');
const cpuStat = require('cpu-stat');
const moment = require('moment');
require('moment-duration-format');

this.run = (lucia, message, extend) => {
	return cpuStat.usagePercent(async (err, percent, seconds) => {
		if (err) return console.log(err);
		
		const chunked = chunk(getFields(lucia, percent), 3);
		const msg = await message.channel.send(getEmbed(lucia, chunked[0][0], chunked[0][1], chunked[0][2], 1, chunked.length));
		let index = 0;
		
		await msg.react('⬅');
		await msg.react('➡');
		async function awaitReactions(){
			const filter = (rect, usr) => ['⬅', '➡'].includes(rect.emoji.name) && usr.id === message.author.id;
			const response = await msg.awaitReactions(filter, {time: 30000, max: 1});
			if(!response.size) return;
			const emo = response.first().emoji.name;
			if(emo === '⬅') index--;
			if(emo === '➡') index++;
			index = ((index % chunked.length) + chunked.length) % chunked.length;
			await msg.edit(getEmbed(lucia, chunked[index][0], chunked[index][1], chunked[index][2], index+1, chunked.length));
			return awaitReactions();
		}
		return awaitReactions();
	});
}

function getEmbed(lucia, fieldOne, fieldTwo, fieldThree, page, total){
	const embed = new RichEmbed()
	.setAuthor(`**Statistics (${page} / ${total})**`, lucia.user.displayAvatarURL)
	.setTitle(`🗜 ${lucia.config.nickname} Statistics 🗜`)
	.setDescription(`\`\`\`A statistics monitoring module. Contains essentials information regarding our service and lucia information. Wrapped with beautiful Discord.js Interactive Library and RichEmbed amazing Constructor. Made with  love, by Lolization Support Team. \`\`\``)
	.setThumbnail(lucia.user.displayAvatarURL)
	.setFooter('This bot is powered by Lolization LLC', 'http://download.lolization.space/pict/loli54.png')
	.setTimestamp()
	.setColor('#15f153');
	
	if(fieldOne) embed.addField(fieldOne.name, fieldOne.value);
	if(fieldTwo) embed.addField(fieldTwo.name, fieldTwo.value);
	if(fieldThree) embed.addField(fieldThree.name, fieldThree.value);
	
	return embed;
}

function getFields(lucia, percent){
	const duration = moment.duration(lucia.uptime).format(' D [days], H [hrs], m [mins], s [secs]');
	
	return new RichEmbed()
	.addField('**Project Leader**', '💠 Riichi_Rusdiana#6815')
	.addField('**Developer**', '💠 MazureFinnson#5492', true)
	.addField('**Contributor**', '💠 OwO#8287 \n💠 Sharif#9781', true)
	.addField('**Server Information**', `\`\`\`
	• Operating System: Enterprise Linux 7 
	• Kernel: 4.18.0-34-Enterprise
	• Processor: Intel(R) Xeon(R) Gold 6140 & EPYC™ 7601 
	• Architecture: x86_x64 
	• Node.js: ${process.version} 
	• Discord.js: v${version} 
	• Apache: Apache/2.4.29 
	• PHP: 7.2.10-0.Enterprise 
	• phpMyAdmin: 4.6.6rpm5 
	• Database: 10.1.34-MariaDB\`\`\``)
	.addField('**Usage Information**', `\`\`\`
	• Memory: ${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB/16384 MB
	• CPU: ${percent.toFixed(2)} %\`\`\``)
	.addField('**Uptime**', `${duration}`)
	.addField('**Created at**', 'Sat Aug 04 2018', true)
	.addField('**Support Us**', `[Paypal](${process.env.PAYPAL})`, true)
	.addField('**More info**', '[Official Website](https://anti-rainbot.lolization.space/)', true)
	.fields
}

function chunk (arr, chunkSize) {
	const temp = [];
	for(let i = 0; i < arr.length; i+= chunkSize){
		temp.push(arr.slice(i, i+chunkSize));
	}
	return temp;
}

this.info = {
	name: 'botstats',
	description: 'Display bot statistic and usage',
	usage: 'botstats',
	aliases: ['stats', 'bstat', 'bst', 'botinfo', 'bin'],
	ownerOnly: false,
	authorPerm: [],
	luciaPerm: ['EMBED_LINKS']
}