const { RichEmbed } = require('discord.js');

exports.run = async (lucia, message, extend) => {
	try{
		let start = Date.now(); message.channel.send(':ping_pong:').then(message => { 
			message.delete();
		  	let diff = (Date.now() - start); 
		  	let API = (lucia.ping).toFixed(2)
			let pingembed = new RichEmbed()
			.setAuthor("**Ping**", lucia.user.displayAvatarURL)
			.setFooter('This bot is powered by Lolization LLC', 'http://download.lolization.space/pict/loli54.png')
		  	.setTitle(`:ping_pong: Pong!`)
		  	.setColor(`RANDOM`)
		  	.addField('Latency', `${diff}ms`, true)
		  	.addField('API', `${API}ms`, true)
		  	return message.channel.send(pingembed);
	  	});
	}catch(err){
		return message.channel.send(`an error occured \`\`\`${err.stack}\`\`\``);
	}
}

exports.info = {
	name: 'ping',
	description: 'Ping pong with the bot',
	usage: 'ping, test',
	aliases: [],
	ownerOnly: false,
	authorPerm: [],
	luciaPerm: [],
}