const session  = require('express-session');
const passport = require('passport');
const Strategy = require('passport-discord');
const cookieParser = require('cookie-parser');

module.exports = (lucia, socket) => {
	passport.serializeUser(function(user, done) {
		done(null, user);
	});
	passport.deserializeUser(function(obj, done) {
		done(null, obj);
	});
	
	var scopes = ['identify', 'email', /* 'connections', (it is currently broken) */ 'guilds', 'guilds.join'];
	
	passport.use(new Strategy({
		clientID: process.env.CLIENT_ID,
		clientSecret: process.env.CLIENT_SECRET,
		callbackURL: `${lucia.config.dns}/callback`,
		scope: scopes
	}, function(accessToken, refreshToken, profile, done) {
		process.nextTick(function() {
			return done(null, profile);
		});
	}));
	socket.app.use(cookieParser());
	socket.app.use(session({
		secret: 'lucia kawaii',
		resave: false,
		saveUninitialized: false
	}));
	socket.app.use(passport.initialize());
	socket.app.use(passport.session());
	socket.app.get('/login', passport.authenticate('discord', { scope: scopes }), function(req, res) {});
	socket.app.get('/callback',
		passport.authenticate('discord', { failureRedirect: '/' }), function(req, res) { res.redirect('/profile') } // auth success
	);
	socket.app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
	socket.app.get('/api/discord/info', checkAuth, function(req, res) {
		res.json(req.user);
	});
	
	
	function checkAuth(req, res, next) {
		if (req.isAuthenticated()) return next();
		res.send({result: 'You\'re not logged in'});
	}
}