require('dotenv').config()

const { config, processHandler } = require('./index.js')
const { Commands, Aliases } = require('./libs/moduleHandler.js');
const { Client } = require('discord.js');

class Lucia extends Client {
	constructor(...args){
		super(...args);
		this.aliases = Aliases;
		this.commands = Commands;
		this.config = config;
        this.token = process.env.TOKEN;
		this.prefix = process.env.PREFIX;
		processHandler()
	}
}

module.exports = Lucia;