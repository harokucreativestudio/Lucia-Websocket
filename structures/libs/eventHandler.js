require('dotenv').config()
const { readdirSync } = require('fs');

module.exports = (lucia, frameSocket) => {
	const events = readdirSync('./structures/events');
	for(const event of events){
		const name = event.split('.')[0];
		const cuteness = require(`../events/${event}`);
		lucia.on(name, (...args) => cuteness(lucia, frameSocket, ...args));
	}
}