const baseDir = require('./../../src/pathfinder.js').baseDir;
const winston = require('winston');

const twoDigit = '2-digit';
const dateFormat = {
  	day: twoDigit,
  	month: twoDigit,
  	year: twoDigit,
  	hour: twoDigit,
  	minute: twoDigit,
  	second: twoDigit
};
function formatter(args) {
  	var dateTimeComponents = new Date().toLocaleTimeString('en-us', dateFormat).split(',');
  	var logMessage = dateTimeComponents[0] + dateTimeComponents[1] + ' - ' + args.level + ': ' + args.message;
  	return logMessage;
}

const options = {
	file: {
		level: 'info',
		filename: `${baseDir}/logs/app.log`,
		handleExceptions: true,
		json: true,
		maxsize: 5242880,
		maxFiles: 5,
		colorize: false,
		formatter: formatter
	},
	console: {
		level: 'debug',
		handleExceptions: true,
		json: true,
		colorize: true,
		formatter: formatter
	},
};


const logger = winston.createLogger({
	transports: [
		new winston.transports.File(options.file),
		new winston.transports.Console(options.console)
	],
	exitOnError: false,
});

logger.stream = {
	write: function(message, encoding) {
		logger.info(message);
	},
};

module.exports = logger;