require('dotenv').config()
const { Collection } = require('discord.js');
const { readdirSync } = require('fs');
const { join } = require('path');

const Commands = new Collection();
const Aliases = new Collection();

const modules = readdirSync(join(__dirname, '../commands'));
console.log(`${modules.length} modules detected, Singularity startup to sync Websocket and Lucia.`);

for(const module of modules){
    console.log(`${module} module loaded!`);
    const commandFile = readdirSync(join(__dirname, '../commands', module));
    for(const file of commandFile){
        let plugin = require(`../commands/${module}/${file}`);
        console.log(`Loading command ${plugin.info.name}`);
        plugin.info.category = module;
        Commands.set(plugin.info.name.toLowerCase(), plugin);
        for(const alias of plugin.info.aliases){
            Aliases.set(alias.toLowerCase(), plugin.info.name.toLowerCase());
        }
    }
}

module.exports = { Commands, Aliases }