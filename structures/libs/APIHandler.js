require('dotenv').config()
const { readdirSync } = require('fs');
const { join } = require('path');

module.exports = (lucia, frameSocket) => {
	const allMethod = Object.keys(frameSocket.app);
	const reqMethod = readdirSync('./structures/api');
	
	console.log(`${reqMethod.length} API method detected, Singularity startup to sync Websocket and Lucia.`);
	
	for(const method of reqMethod){
		console.log(`Request handler for ${method} method loaded!`);
		const apis = readdirSync(join('./structures/api', method));
		for(const file of apis){
			let api = require(`../api/${method}/${file}`);
			console.log(`Loading command ${api.info.name}`);
			if(!allMethod.includes(method.toLowerCase())) throw TypeError(`Only support: ${allMethod.map(x => x.toUpperCase()).join(', ')}`);
			frameSocket.app[method.toLowerCase()](`/api${api.info.endpoint}`, (req, res) => {
				try {
					api.route(lucia, frameSocket, api.info, req, res);
					console.log(`${lucia.config.nickname} ${api.info.name}/${api.info.method} RESTful API with endpoint of ${api.info.endpoint} is getting requested..`);
				} catch(err) {
					res.sendStatus(404);
				}
			});
		}
	}
	// const reqMethod = readdirSync('./structures/api');
	// console.log(`${reqMethod.length} API method detected, Singularity startup to sync Websocket and Lucia.`);

	// for(const method of reqMethod){
	// 	console.log(`Request handler for ${method} method loaded!`);
	// 	const apis = readdirSync(join('./structures/api', method));
	// 	for(const file of apis){
	//     	let api = require(`../api/${method}/${file}`);
	//     	console.log(`Loading command ${api.info.name}`);
	// 		api.info.method = method;
	// 		if(api.info.method == 'GET') {
	// 			frameSocket.app.get(`/api${api.info.endpoint}`, (req, res) => {
	// 				try {
	// 					api.route(lucia, frameSocket, api.info, req, res);
	// 					console.log(`${lucia.config.nickname} ${api.info.name}/${api.info.method} RESTful API with endpoint of ${api.info.endpoint} is getting requested..`);
	// 				} catch(err) {
	// 					res.sendStatus(404);
	// 				}
	// 			})
	// 		} else if(api.info.method == 'POST') {
	// 			frameSocket.app.post(`/api${api.info.endpoint}`, (req, res) => {
	// 				try {
	// 					api.route(lucia, frameSocket, api.info, req, res);
	// 					console.log(`${lucia.config.nickname} ${api.info.name}/${api.info.method} RESTful API with endpoint of ${api.info.endpoint} is getting requested..`);
	// 				} catch(err) {
	// 					res.sendStatus(404);
	// 				}
	// 			})
	// 		} else if(api.info.method == 'PUT') {
	// 			frameSocket.app.put(`/api${api.info.endpoint}`, (req, res) => {
	// 				try {
	// 					api.route(lucia, frameSocket, api.info, req, res);
	// 					console.log(`${lucia.config.nickname} ${api.info.name}/${api.info.method} RESTful API with endpoint of ${api.info.endpoint} is getting requested..`);
	// 				} catch(err) {
	// 					res.sendStatus(404);
	// 				}
	// 			})
	// 		} else if(api.info.method == 'DELETE') {
	// 			frameSocket.app.delete(`/api${api.info.endpoint}`, (req, res) => {
	// 				try {
	// 					api.route(lucia, frameSocket, api.info, req, res);
	// 					console.log(`${lucia.config.nickname} ${api.info.name}/${api.info.method} RESTful API with endpoint of ${api.info.endpoint} is getting requested..`);
	// 				} catch(err) {
	// 					res.sendStatus(404);
	// 				}
	// 			})
	// 		}
	// 	}
	// }
}
