const dbConfig = require('../database.config.js');
const database = require('mongoose');

database.Promise = global.Promise;

database.connect(dbConfig.Mongourl, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the Mongo database.', err);
});