require('dotenv').config()
const { readdirSync } = require('fs');

module.exports = (lucia, frameSocket) => {
	const routes = readdirSync('./structures/routes');
	console.log(`${routes.length} routes detected, Singularity startup to sync Websocket and Lucia.`);
	for(const route of routes){
		const gateway = require(`../routes/${route}`);
		console.log(`Websocket route ${gateway.info.name} loaded.`);
		frameSocket.app.get(`/${gateway.info.route}`, (req, res) => {
            try {
				// req.params.type = gateway.info.route;
                gateway.route(lucia, frameSocket, gateway.info, req, res);
            } catch(err) {
                res.render('error', { title: `${lucia.config.nickname} Error`, errType: `404 Route Not Found` });
            }
        })
	}
}