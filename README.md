# Lucia-Websocket
A Websocket for Express, written in Nodejs (Absolutely) with Express-Handlebars and Various DB Engine.
Migrated from Legacy Kano-Websocket, see here [https://github.com/BillyAddlers/Kano-Websocket](https://github.com/BillyAddlers/Kano-Websocket).

## .env File Content
Use it for a reference about how to config .env file
```css
// Discord Evaluator Prefix
PREFIX=
// Mysql DB Host
DB_HOST=
// Mysql DB Username
DB_USER=
// Mysql DB Password
DB_PASS=
// App Client ID
CLIENT_ID=
// App Client Secret
CLIENT_SECRET=
// Discord Bot Token
TOKEN=
// Server Hostname (Fill it with your FQDN Hostname)
HOSTNAME=
// Paypal Link
PAYPAL=
// Google API Key
GOOGLE_API=
```

## Documentation coming soon
[https://docs.lolization.space/project/lucia](UwU).
