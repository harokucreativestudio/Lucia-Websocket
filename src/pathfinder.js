const path = require('path');

const baseDir = path.join(__dirname, '../'); // grab Root Directory
const library = path.join(baseDir, 'structures/index.js');
const app = path.join(baseDir, 'app.js');
const config = path.join(baseDir, 'config.json');

module.exports = {
	baseDir: baseDir,
	library: library,
	app: app,
	config: config,
};